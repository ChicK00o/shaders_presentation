﻿using System;
using UnityEngine;

public class Controller : MonoBehaviour
{
    [SerializeField]
    private PresentationData[] _allDataPoints;

    [SerializeField]
    private Transform _mainCamera;

    private int _currentIndex = 0;

    public void Start()
    {
        _currentIndex = 0;
        ActivateObject(_currentIndex);
    }

    private void ActivateObject(int currentIndex)
    {
        foreach (PresentationData data in _allDataPoints)
        {
            data.GO.SetActive(false);
        }
        _allDataPoints[currentIndex].GO.SetActive(true);
        _mainCamera.position = _allDataPoints[currentIndex].CameraPosition;
        _mainCamera.rotation = Quaternion.Euler(_allDataPoints[currentIndex].CameraRotation);
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.LeftArrow))
        {
            if (_currentIndex > 0)
            {
                _currentIndex--;
                ActivateObject(_currentIndex);
            }
        }

        if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.RightArrow))
        {
            if (_currentIndex + 1 < _allDataPoints.Length)
            {
                _currentIndex++;
                ActivateObject(_currentIndex);
            }

        }
    }
}

[Serializable]
public class PresentationData
{
    [SerializeField]
    public GameObject GO;

    [SerializeField]
    public Vector3 CameraRotation;

    [SerializeField]
    public Vector3 CameraPosition;
}
