﻿using UnityEngine;

[ExecuteInEditMode]
public class ColorSetter : MonoBehaviour
{

    [SerializeField]
    private Color _applyColor;

    [SerializeField]
    private Renderer _renderer;
    private MaterialPropertyBlock _propBlock;

    [SerializeField]
    private bool _valeChanged = false;

    void Update ()
    {
//        if (!_valeChanged) return;
//        _valeChanged = false;

        if (_propBlock == null) _propBlock = new MaterialPropertyBlock();
        // Get the current value of the material properties in the renderer.
        _renderer.GetPropertyBlock(_propBlock);
        // Assign our new value.
        _propBlock.SetColor("_Color", _applyColor);
        // Apply the edited values to the renderer.
        _renderer.SetPropertyBlock(_propBlock);
    }
}
