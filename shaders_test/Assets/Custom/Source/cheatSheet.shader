﻿Shader "custom/cheatSheet"
{
    Properties
    {
        //[PerRendererData] - indicates that a [texture] property will be coming from per-renderer data in the form of a MaterialPropertyBlock.
        //Material inspector changes the texture slot UI for these properties

        //[Normal] - indicates that a texture property expects a normal-map

        //[NoScaleOffset] - material inspector will not show texture tiling/offset fields for texture properties with this attribute

        //[HideInInspector] - does not show the property value in the material inspector.

        //Range - Float (Range this for limitation)
        //Color - Vector
        //2d Texture
        //float > half > fixed
        //variable name ("[display name]", TYPE)
        _MainTex ("Texture", 2D) = "white" {}

        //_ScrFactor ("Source Blend Factor", float) = 0.5
        //_DestFactor ("Destination Blend Factor", float) = 0.5
    }
    SubShader
    {
        //Directive and value setting for the current shader
        //"Queue" = "Background" < "Geometry" < "AlphaTest" < "Transparent" < "Overlay"
        //"RenderType" = "Opaque" | "Transparent" | "Overlay" .... used for mass replacement of shaders
        //"DisableBatching" = "False" (default) | "True" | "LODFading"
        //"ForceNoShadowCasting" = "True" | "False" to Disable shadow casting
        //"IgnoreProjector" = "True" | "False"
        //"CanUseSpriteAtlas" = "True" | "False"
        Tags { "RenderType"="Opaque" }

        // Grab the screen behind the object into _BackgroundTexture
        //GrabPass { "_BackgroundTexture" }
        //Vertex function => Vertex position by
        //  ComputeGrabScreenPos(o.vertex); where vertex is now screen pos using MVP
        //fragment function => get color data by
        //  tex2Dproj(_BackgroundTexture, UNITY_PROJ_COORD([vertex pos above]));
        //      UNITY_PROJ_COORD Given a 4-component vector, this returns a Texture coordinate suitable
        //                          for projected Texture reads. On most platforms this returns the given value directly.

        //For Face Culling
        Cull Back //(Default) => Dont Render Away from Viewer
        //Front => Dont Render towards the Viewer [Inside Out]
        //Off => potentially faces nearest drawn

        ZTest LEqual //(Default) => draw objects in from or at the distance as existing objects; hide objects behind them
        //Less | Greater | GEqual | Equal | NotEqual | Always

        //Offset Factor, Units
        //Factor scales the maximum Z slope, with respect to X or Y of the polygon, and units scale the minimum resolvable depth buffer value
        //Offset 0, –1 pulls the polygon closer to the camera ignoring the polygon’s slope, whereas Offset –1, –1 will pull the polygon even closer when looking at a grazing angle
        //CanUseSpriteAtlas be used for avoiding depth fighting

        //For Disabling Sub shader using Shader.maximumLOD OR
        // Disable Sub shaders across the system using Shader.globalmaximumLOD
        LOD 100

        ZWrite On //(default) | Off
        // the depth buffer keeps the depth of the nearest fragment and discards any fragments
        //that have a larger depth. In the case of a transparent fragment, however,
        //this is not what we want since we can (at least potentially) see through a
        //transparent fragment. Thus, transparent fragments should not occlude other
        //fragments and therefore the writing to the depth buffer is deactivated

        //Blend is a command for the Grphic pipeline to understand how to handle it.
        //Blending is the last step in the graphic pipeline
        //Blend [_ScrFactor] [_DesFactor]
        Blend SrcAlpha OneMinusSrcAlpha // Default blending
        //Blend ScrColor DestColor, ScrAlpha DestAlpha
        //One | Zero | ScrColor | ScrAlpha | DstColor | DstAlpha | OneMinusScrColor | OneMinusSrcAlpha
        //OneMinusDstColor | OneMinusDstAlpha
        //Scr => fragment_output | Dst => current pixel_color in the frameBuffer

        //OR

        //BlendOp Op => Operation
        //BlendOp ColorOp, AlphaOp
        //Add | Sub => Source-Dest | RevSub => Dest-Source | Min | Max

        //OR

        //AlphaToMask On => use for cutout Alpha test and use of anti-aliasing for results mid of Blend and clip()

        Pass
        {
            CGPROGRAM
            // compile function name as the vertex shader.
            #pragma vertex vertFunctionName
            // compile function name as the fragment shader.
            #pragma fragment fragFunctionName

            //HLSLSupport .cginc is Auto imported
            //defines tex2D fucntions and most of the semantics the gpu uses
            //UnityShaderVariables .cginc is Auto imported
            //has most functions related to ligthing and reflections to be used
            //Generally included Always, has default struct data defined in it
            #include "UnityCG.cginc" //UnityCG

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;

            //float4 with "_ST" suffix. The x,y contains texture scale, and z,w contains translation (offset)
            //Needed by TRANSFORM_TEX
            //Auto populated by Unity3d
            float4 _MainTex_ST;

            //Vertex shader input semantics
            //POSITION = SV_POSITION [DirectX equivalent] is the vertex position, typically a float3 or float4.
            //                          Vertex position in Object space
            //NORMAL is the vertex normal, typically a float3.
            //          Relative to the world
            //TEXCOORD0 is the first UV coordinate, typically float2, float3 or float4.
            //TEXCOORD1, TEXCOORD2 and TEXCOORD3 are the 2nd, 3rd and 4th UV coordinates
            //          these can be used for additional data. eg Zapper
            //TANGENT is the tangent vector (used for normal mapping), typically a float4.
            //COLOR is the primary per-vertex color, typically a float4
            //      COLOR = COLOR0, DIFFUSE, SV_TARGET
            //COLOR1, SPECULAR is secondary color information

            //possible default input data
            //appdata_base: position, normal, one texture coordinate.
            //appdata_tan: position, normal, tangent, one texture coordinate.
            //appdata_full: position, normal, tangent, vertex color and four texture coordinates.
            //appdata_img: with position and one texture coordinate.

            //  float4 vertex : POSITION;
            //  float4 tangent : TANGENT;
            //  float3 normal : NORMAL;
            //  float4 texcoord : TEXCOORD0;
            //  float4 texcoord1 : TEXCOORD1;
            //  float4 texcoord2 : TEXCOORD2;
            //  float4 texcoord3 : TEXCOORD3;
            //  fixed4 color : COLOR;

            //defined in UnityCG.cginc


            v2f vertFunctionName (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);

                //#define TRANSFORM_TEX(tex,name) (tex.xy * name##_ST.xy + name##_ST.zw)
                //_MainTex => _MainTex_ST
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            //Vertex shader output semantics => Fragment shader input
            //POSITION = SV_POSITION is the vertex position, typically a float3 or float4.
            //                          Vertex position in Camera coordinates
            //                          clip space from 0 - 1 for each dimension
            //TEXCOORD0 is the first UV coordinate, typically float2, float3 or float4.
            //TEXCOORD1, TEXCOORD2 and TEXCOORD3 are the 2nd, 3rd and 4th UV coordinates
            //          these can be used for additional data. eg Zapper
            //COLOR is the Front primary color, typically a float4
            //      COLOR = COLOR0, COL0, COL, SV_TARGET
            //COLOR1, COL1 is Front secondary color information

            //possible default input data to fragment
            //v2f_img : position and one texture coordinate
            //  v2f_img vert_img( appdata_img v )
            //  {
            //      v2f_img o;
            //      o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
            //      o.uv = v.texcoord;
            //      return o;
            //  }
            //v2f_vertex_lit : TEXCOORD0 as UV, COLOR0 as DIFFUSE, COLOR1 as specular
            fixed4 fragFunctionName (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);
                return col;
            }
            ENDCG
        }
    }
}

/*

    UNITY_MATRIX_MVP        Current model * view * projection matrix.
    UNITY_MATRIX_MV         Current model * view matrix.
    UNITY_MATRIX_V          Current view matrix.
    UNITY_MATRIX_P          Current projection matrix.
    UNITY_MATRIX_VP         Current view * projection matrix.
    UNITY_MATRIX_T_MV       Transpose of model * view matrix.
    UNITY_MATRIX_IT_MV      Inverse transpose of model * view matrix.
    _Object2World           Current model matrix.
    _World2Object           Inverse of current world matrix.

*/

/*

    _WorldSpaceCameraPos                float3      World space position of the camera.
    _ProjectionParams                   float4      x is 1.0 (or –1.0 if currently rendering with a flipped projection matrix), y is the camera’s near plane, z is the camera’s far plane and w is 1/FarPlane.
    _ScreenParams                       float4      x is the camera’s render target width in pixels, y is the camera’s render target height in pixels, z is 1.0 + 1.0/width and w is 1.0 + 1.0/height.
    _ZBufferParams                      float4      Used to linearize Z buffer values. x is (1-far/near), y is (far/near), z is (x/far) and w is (y/far).
    unity_OrthoParams                   float4      x is orthographic camera’s width, y is orthographic camera’s height, z is unused and w is 1.0 when camera is orthographic, 0.0 when perspective.
    unity_CameraProjection              float4x4    Camera’s projection matrix.
    unity_CameraInvProjection           float4x4    Inverse of camera’s projection matrix.
    unity_CameraWorldClipPlanes[6]      float4      Camera frustum plane world space equations, in this order: left, right, bottom, top, near, far.

*/

/*

    _Time               float4      Time since level load (t/20, t, t*2, t*3), use to animate things inside the shaders.
    _SinTime            float4      Sine of time: (t/8, t/4, t/2, t).
    _CosTime            float4      Cosine of time: (t/8, t/4, t/2, t).
    unity_DeltaTime     float4      Delta time: (dt, 1/dt, smoothDt, 1/smoothDt).

*/

/*

    float4 UnityObjectToClipPos(float3 pos)     Transforms a point from object space to the camera’s clip space in homogeneous coordinates. This is the equivalent of mul(UNITY_MATRIX_MVP, float4(pos, 1.0)), and should be used in its place.
    float3 UnityObjectToViewPos(float3 pos)     Transforms a point from object space to view space. This is the equivalent of mul(UNITY_MATRIX_MV, float4(pos, 1.0)).xyz, and should be used in its place.

*/


/*

    Vertex Shader
    positioning the model — modeling transformation
    positioning the camera — viewing transformation
    adjusting the zoom — projection transformation
    =======
    cropping the image — viewport transformation

    object/model coordinates => input paramerters semantics POSITION
        model matrix => matrix object to world
    world coordinates
        view matrix => matrix world to view
    view coordinates
        projection matrix
    vertix fragment_output with semantics SV_POSITION

*/






/*



*/