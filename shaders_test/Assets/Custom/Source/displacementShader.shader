﻿Shader "Unlit/Distortion" {
    Properties {
        _MainTex ("Base (RGB)", 2D) = "white" {}
        //_bwBlend ("Black & White blend", Range (0, 1)) = 0
    }
    SubShader {
        Tags { "RenderType"="Opaque" }

        Cull Off

        Pass {
            CGPROGRAM
                #pragma vertex vert
                #pragma fragment frag
                #include "UnityCG.cginc"

                struct v2f {
                    float4 pos : SV_POSITION;
                    float2 uv_MainTex : TEXCOORD0;
                };

                float4 _MainTex_ST;
                //float _bwBlend;

                v2f vert(appdata_base v) {
                    v2f o;
                    //o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
                    //Gaeel's vertex modification starts here...
                    float4 pos = mul(UNITY_MATRIX_MV, v.vertex);

                    //float distanceSquared = pos.x * pos.x + pos.z * pos.z;
                    //pos.y += 5*sin(distanceSquared*_SinTime.x/50);
                    //float y = pos.y;
                    //float x = pos.x;
                    //float om = sin(distanceSquared*_SinTime.x/100) * _SinTime.x;
                    //pos.y = x*sin(om)+y*cos(om);
                    //pos.x = x*cos(om)-y*sin(om);

                    o.pos = mul(UNITY_MATRIX_P, pos);
                    //...and ends here.

                    o.uv_MainTex = TRANSFORM_TEX(v.texcoord, _MainTex);
                    return o;
                }

                sampler2D _MainTex;

                float4 frag(v2f IN) : COLOR {
                    float4 c = tex2D (_MainTex, IN.uv_MainTex);

                    //float lum = c.r*.3 + c.g*.59 + c.b*.11;
                    //float3 bw = float3( lum, lum, lum );

                    float4 result = c;
                    //result.rgb = lerp(c.rgb, bw, _bwBlend);
                    return result;

                    //return c;
                }
            ENDCG
        }
    }
}